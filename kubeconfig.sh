# This script extract the information needed to integrate
# a Kubernetes cluster with a GitLab project
# ---

function usage {
  echo $1
  echo
  echo "This script extracts cluster information to integrate with GitLab"
  echo "usage: kubeconfig.sh"
  exit 0
}

function pre-check {
  # Make sure jq is there
  jq --help 1>/dev/null 2>&1 || usage "please install jq first (https://stedolan.github.io/jq/download/)"

  # Make sure kubectl is correctly configured
  kubectl version --request-timeout='4' 1>/dev/null 2>&1 || usage "Please install and configure kubectl first"
}

# RBAC rules creation
function rbac {
  kubectl apply -f -<<EOF
apiVersion: v1
kind: ServiceAccount
metadata:
  name: gitlab-admin
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: gitlab-admin
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: gitlab-admin
  namespace: kube-system
EOF
}

# Get cluster name from config
function cluster-name {
  echo -e "-> Cluster name: [$(kubectl config view --minify -o jsonpath={.current-context})]\n"
}

# Get API Server URL from config
function cluster-endpoint {
  API_URL=$(kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."server"')
  echo -e "-> API URL: [$API_URL]\n"
}

# Get secret used by the service account
function service-token {
  SECRET=$(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')
  TOKEN=$(kubectl -n kube-system get secret $SECRET -o jsonpath='{.data.token}' | base64 --decode)
  echo -e "-> Service token: [$TOKEN]\n"
}

# Get cluster CA
function cluster-ca {
  kubectl config view --raw -o json | jq -r '.clusters[] | select(.name == "'$(kubectl config current-context)'") | .cluster."certificate-authority-data"' | base64 --decode > cluster_ca.pem
  echo "-> Cluster CA: "
  cat cluster_ca.pem
  rm cluster_ca.pem
}

pre-check
rbac 1>/dev/null || usage "Cannot create RBAC rules"
cluster-name || usage "Cannot retrieve cluster name"
cluster-endpoint || usage "Cannot get api-server endpoint"
service-token || usage "Cannot get service token"
cluster-ca || usage "Cannot get cluster CA"
